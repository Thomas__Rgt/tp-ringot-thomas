/*----------------------------------------------------------------
nom du porgramme:   app_gerer_capt
utilité:    ce programme va servir à:   -stocker dans la bdd l'@MAC des capteurs de place 
                                        et les configurer en attribuant à chacun d'eux un ID unique qui leurs sera renvoyé
                                        
                                        -stocker l'etat des places en fonction de la valeur envoyée par le capteur (avec mise à jour de l'historique des places dans la bdd(logoccupation))

                                        -Envoi lors de chaque changement d'etat d'une place le nombre de place disponible dans chaque etage aux afficheurs locaux

                                        -( l'association d'une place à un capteur est fait à la main dans la bdd )

Auteur: Ringot Thomas

date de la derniere mise à jour:20/03/2020
----------------------------------------------------------------*/


//-------------------------CONSTANTE------------------------------
//constante pour la connexion MQTT 
const ADDR_SERV_MQTT = 'mqtt://192.168.1.37';
//const ADDR_SERV_MQTT = 'mqtt://172.16.129.22';
const PORT_MQTT = 1883;
const USER_MQTT = '';
const MDP_MQTT = '';

//constante pour la connexion à la base de donnée local
const ADDR_HOTE_BDD = 'localhost';
const USER_BDD = 'root';
const MDP_BDD = 'raspberry';
const NOM_BDD = 'bddparkinglocal'

//constante pour les Topics

/*--------Premiere configuration---------*/
//pour la première configuration @MAC -> BDD
const T_GIVE_ME_MAC = 'giveMAC';
//pour la première configuration BDD -> ID
const T_GIVE_ID = 'giveID'

/*--------Phase Nominale---------*/
//pour l'etat des places (libre ou occupé)
const T_OCCUPATION_PLACE = 'occupationPlace';

//pour afficher sur les afficheurs
const T_AFF_LOCAUX = 'afficheursLocaux';

//topic de test
const T_TEST = 'test'

//-----------------------INCLUDES--------------------------

//inclure MQTT
var mqtt = require('mqtt');

//inclure MySQL
var mysql = require('mysql');

//-----------------------------VARIABLES------------------------------


//---------------------CREATION DES CLIENTS----------------------------

//création d'un client pour se connecter au serveur sur un cerain port et avec un nom d'utilisateur et un mot de passe en mqtt
var client = mqtt.connect(ADDR_SERV_MQTT)

//création d'une connexion mysql pour pouvoir utiliser la BDD
var mysqlConnection = mysql.createConnection({
    host: ADDR_HOTE_BDD,
    user: USER_BDD,
    password: MDP_BDD,
    database: NOM_BDD
});

//--------------------------CONNEXIONS ETABLIES------------------------

//lorsque la connexion avec la BDD est établie:
mysqlConnection.connect(function () {
    console.log("Connexion avec la bdd établie!");      //affichage message
});


//lorsque la connexion avec le serveur MQTT est établie: 
client.on('connect', function () {
    console.log("Connexion au serveur MQTT établi!");


    //on s'abonne au topic giveMAC pour récupérer l'adresse MAC du capteur               MAC -> ID
    client.subscribe(T_GIVE_ME_MAC, function (err) {

        //si il y a une erreur
        if (err) console.log("erreur lors de l'abonnement du topic" + T_GIVE_ME_MAC);

    })

    //on s'abonne au topic occupationPlace pour récupérer l'etat de la place du capteur     OCCUPATION PLACE
    client.subscribe(T_OCCUPATION_PLACE, function (err) {

        if (err) console.log("probleme d'abonnement au topic test")

    })

})

//--------------------RECEPTION DE MESSAGE----------------------
//lorsque l'on recoit un message:
client.on('message', function (topic, message) {

    //si le topic sur lequel le message est publié est T_GIVE_ME_MAC                    /*-----PENSER A SUPPRIMER L4ETAT DE LA PLACE, PUISQU4IL S4AGIT D4UNE NOUVELLE PLACE-----*/
    if (topic === T_GIVE_ME_MAC) {

        //variable contenant l'adresse MAC recu en MQTT (message)
        var addMAC = message.toString();    //stocker dans addMAC l'adresse MAC recu en MQTT 

        //on affiche le topic
        console.log("le topic est: ");
        console.log(topic.toString());

        //on affiche l'adresse MAC
        console.log("l'adresse MAC est: ");
        console.log(addMAC);

        //on stock dans la bdd l'adresse MAC envoyé par le capteur si elle n'existe pas deja 
        //on regarde si l'addrMAC recu en MQTT existe dans la bdd
        mysqlConnection.query("SELECT addrMAC FROM capteur WHERE addrMAC = '" + addMAC + "' ", function (err, result) {

            //variable contenant le resultat de la requete mysql
            var resAddMac = result[0];  //on stocke le resultat de la requete mysl dans resAddMac


            //resultat de la requete:
            console.log("le resultat de la requete est: ");
            console.log(resAddMac);

            //si l'adresse MAC n'est pas stockée ds la bdd,
            if (resAddMac == undefined) {

                //affichage console
                console.log("l\'adresse MAC du capteur n\'est pas enregistrée dans la bdd...");

                //On stocke l'adresse dans la bdd et on lui attribue une place
                //insertion de l'adresse MAC dans la bdd (avec date d'association)
                mysqlConnection.query("INSERT INTO capteur (addrMAC, dateAssoc) VALUES ('" + addMAC + "',DATE( NOW() ) )", function (err, result) {
                    if (err) console.log(err);
                    console.log("adresse MAC enregistrée");

                    //attribution de la place fait à la main dans la bdd
                });



            } else {        //si l'adresse MAC existe deja dans la bdd

                //affichage console
                console.log("l\'adresse MAC du capteur existe deja dans la bdd");

                //on laisse l'adresse du capteur dans la bdd mais on change le nom de sa place (fait a la main)

            }
            /*---------------On renvoie ensuite l'ID correspondant a l'adresse MAC---------------*/
            //On récupère l'ID correspondant dans la bdd
            mysqlConnection.query("SELECT idCapteur FROM capteur WHERE addrMAC = '" + addMAC + "'", function (err, result) {
                //variable contenant le resultat de la requete mysql (ID)
                var resID = result[0];  //on stocke le resultat de la requete mysl dans resID

                //variable pour utiliser la valeur récupéré par la requete (et non pas la valeur de la requete entière)
                var valresID = resID.idCapteur

                //convertie en string
                valresID = valresID.toString()

                console.log("valResID")
                console.log(valresID);

                //On publie sur ce topic L'ID correspondant à l'@MAC (addMAC)
                client.publish(T_GIVE_ID, valresID)
            });
        });
    }



    //si le topic sur lequel le message est publié est T_OCCUPATION_PLACE           (MESSAGE EN JSON)
    if (topic === T_OCCUPATION_PLACE) {

        var idCapteur = message     //variable contenant lID du capteur
        var etatPlace = 'libre'           //variable contenant l'etat de la place envoyé depuis le capteur
        var estLibre             //variable contenant 1  ou  0  en fonction de l'etat le la place
        var nbEtages        //variable contenant le nombre d'etage dans le parking

        //objet qui contiendra la liste des etages du parkings avec l'idEtage et le nombre de place libre dans cet etage
        var stringEtages = {
            etages: []
        }

        //variable qui contiendra l'objet stringEtages convertit au format JSON sous forme de chaine de charactère 
        var stringEtagesJSON = ""


        //Conditions permettant de determiner si la place est libre ou non, en fonction de l'etat de la place
        if (etatPlace === 'occupe') estLibre = 0;
        if (etatPlace === 'libre') estLibre = 1;


        console.log("le message recu est : " + message)

        /*vérification si la place correspondant à l'etat du capteur existe dans la base de données (idPlace) 
        ET est assigné à une occupation de place (dans logoccupation)*/
        mysqlConnection.query("SELECT capteur.idCapteur, place.nom, place.idPlace FROM capteur, place" +
            " WHERE capteur.idCapteur = '" + idCapteur + "'  AND capteur.idCapteur = place.idCapteur", function (err, result) {

                //message d'erreur
                if (err) console.log("erreur dans la requete" + err)

                //affichage du résultat
                console.log(result[0])

                //SI l'id du capteur existe et est assigné à une place:
                if (result[0] != undefined) {

                    var idPlace = result[0].idPlace         //variable contenant l'ID de la place du capteur
                    var nomPlace = result[0].nom            //variable contenant le nom de la place

                    //on met a jour l'etat de la place dans place.libre  (1 -> true    /   0 -> false)
                    mysqlConnection.query("UPDATE place SET libre = " + estLibre + " WHERE idPlace = '" + idPlace + "'", function (err, result) {
                        if (err) {
                            console.log("erreur dans la requete" + err)
                        } else {
                            console.log("l'etat de la place: " + nomPlace + " à été mis a jour.")
                        }

                    })

                    //On insert dans la table logOccupation l'etat de la place (afin de garder une trace de l'activité des capteurs) 
                    mysqlConnection.query("INSERT INTO logoccupation (etat, idPlace) VALUES ('" + etatPlace + "','" + idPlace + "')", function (err, result) { })

                    //une fois l'etat de la place modifié, on calcul du nombre de place libre dans chaque etages 

                    //On selectionne l'ID de tous les etages du parking
                    mysqlConnection.query("SELECT idEtage FROM etage", function (err, result) {

                        //pour chaque etage, 
                        for (var i = 0; i < result.length; i++) {

                            //on fait la somme des places dans chaque etages (on utilise place.libre car c'est un booleen, si la place est libre: libre = 1, on peut donc les additionner)
                            mysqlConnection.query("SELECT SUM(place.libre) AS nbPlacesDispo, allee.idEtage FROM allee, etage, place WHERE allee.idAllee = place.idAllee " +
                                "AND etage.idEtage = allee.idEtage AND allee.idEtage = '" + result[i].idEtage + "'", function (err, result2) {

                                    //on rajoute dans stringEtages.etages, l'idEtage et le nb de place dispo dans cette etage         ex:{ idEtage: '2', nbPlaceDispo: '50' } 
                                    stringEtages.etages.push({ idEtage: result2[0].idEtage, nbPlaceDispo: result2[0].nbPlacesDispo })

                                })
                        }
                        //on attend que stringEtages.etage soit complet (boucle for finisse)
                        setTimeout(function () {        //attente de 1.5 seconde

                            //on convertit notre valeur javaScript (stringEtages) en chaine JSON (stringEtagesJSON)
                            stringEtagesJSON = JSON.stringify(stringEtages)

                            //console.log(stringEtagesJSON)

                            //on remplace les " par \" pour ne pas avoir de probleme de lecture du coté afficheur
                            stringEtagesJSON = stringEtagesJSON.replace(/"/g, '~')          // /.../g pour modifier dans toute la chaine de caract. 

                            //on rajoute les "" pour pouvoir le récupérer correctement coté afficheur
                            stringEtagesJSON = "\"" + stringEtagesJSON + "\""

                            //console.log("string qui sera envoyé:  " + stringEtagesJSON)

                            //on publie sur le topic T_AFF_LOCAUX la chaine JSON récupéré
                            //client.publish(T_AFF_LOCAUX, stringEtagesJSON)

                            //test
                            var test = "5{~etages~:[{~idEtage~:2,~nbPlaceDispo~:3},{~idEtage~:3,~nbPlaceDispo~:1},{~idEtage~:-1,~nbPlaceDispo~:-1}]}"
                            client.publish(T_AFF_LOCAUX, test)
                            console.log(stringEtagesJSON)

                        }, 500) //delai de 500ms 
                    })
                } else {                           //SINON, SI l'id du capteur n'existe pas OU n'est pas assigné à une place:

                    //Affichage
                    console.log("Le capteur n'est pas assignée à une place. Pour ce faire, rendez-vous dans la base de données ")
                    console.log("Si le capteur n'est pas enregistré dans la base de données. Appuyer sur le bouton du capteur.")

                }
            });
    }
});




