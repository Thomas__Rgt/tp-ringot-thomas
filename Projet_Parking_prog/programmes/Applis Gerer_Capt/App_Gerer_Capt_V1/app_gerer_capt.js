//-------------------------CONSTANTE------------------------------
//constante pour la connexion MQTT 
const ADDR_SERV_MQTT = 'mqtt://172.16.129.22';
const PORT_MQTT = 1883;
const USER_MQTT = '';
const MDP_MQTT = '';

//constante pour la connexion à la base de donnée local
const ADDR_HOTE_BDD = 'localhost';
const USER_BDD = 'root';
const MDP_BDD = 'raspberry';
const NOM_BDD = 'bddparkinglocal'

//constante pour les Topics

/*--------Premiere configuration---------*/
//pour la première configuration @MAC -> BDD
const T_GIVE_ME_MAC = 'giveMAC';
//pour la première configuration BDD -> ID
const T_GIVE_ID = 'giveID'

/*--------Phase Nominale---------*/
//pour l'etat des places (libre ou occupé)
const T_OCCUPATION_PLACE = 'occupationPlace';

//pour afficher sur les afficheurs
const T_AFF_LOCAUX = 'afficheursLocaux';

//-----------------------INCLUDES--------------------------

//inclure MQTT
var mqtt = require('mqtt');

//inclure MySQL
var mysql = require('mysql');

//-----------------------------VARIABLES------------------------------


//---------------------CREATION DES CLIENTS----------------------------

//création d'un client pour se connecter au serveur sur un cerain port et avec un nom d'utilisateur et un mot de passe en mqtt
var client = mqtt.connect(ADDR_SERV_MQTT)

//création d'une connexion mysql pour pouvoir utiliser la BDD
var mysqlConnection = mysql.createConnection({
    host: ADDR_HOTE_BDD,
    user: USER_BDD,
    password: MDP_BDD,
    database: NOM_BDD
});

//--------------------------CONNEXIONS ETABLIES------------------------

//lorsque la connexion avec la BDD est établie:
mysqlConnection.connect(function () {
    console.log("Connexion avec la bdd établie!");      //affichage message
});


//lorsque la connexion avec le serveur MQTT est établie: 
client.on('connect', function () {
    console.log("Connexion au serveur MQTT établi!");

    //--------------------------ADRESSE MAC -> ID---------------------------
    //on s'abonne au topic giveMAC pour récupérer l'adresse MAC du capteur
    client.subscribe(T_GIVE_ME_MAC, function (err) {

        //si il y a une erreur
        //if(err) console.log("erreur lors de l'abonnement du topic" + T_GIVE_ME_MAC);



        //--------------------RECEPTION----------------------
        //lorsque l'on recoit un message:
        client.on('message', function (topic, message) {

            //si le topic sur lequel le message est publié est T_GIVE_ME_MAC
            if (topic === T_GIVE_ME_MAC) {

                //variable contenant l'adresse MAC recu en MQTT (message)
                var addMAC = message.toString();    //stocker dans addMAC l'adresse MAC recu en MQTT 

                //on affiche le topic
                console.log("le topic est: ");
                console.log(topic.toString());

                //on affiche l'adresse MAC
                console.log("l'adresse MAC est: ");
                console.log(addMAC);

                //on stock dans la bdd l'adresse MAC envoyé par le capteur si elle n'existe pas deja 
                //on regarde si l'addrMAC recu en MQTT existe dans la bdd
                mysqlConnection.query("SELECT addrMAC FROM capteur WHERE addrMAC = '" + addMAC + "' ", function (err, result) {

                    //variable contenant le resultat de la requete mysql
                    var resAddMac = result[0];  //on stocke le resultat de la requete mysl dans resAddMac


                    //resultat de la requete:
                    console.log("le resultat de la requete est: ");
                    console.log(resAddMac);

                    //si l'adresse MAC n'est pas stockée ds la bdd,
                    if (resAddMac == undefined) {

                        //affichage console
                        console.log("l\'adresse MAC du capteur n\'est pas enregistrée dans la bdd...");

                        //On stocke l'adresse dans la bdd et on lui attribue une place
                        //insertion de l'adresse MAC dans la bdd (avec date d'association)
                        mysqlConnection.query("INSERT INTO capteur (addrMAC, dateAssoc) VALUES ('" + addMAC + "',DATE( NOW() ) )", function (err, result) {
                            if (err) console.log(err);
                            console.log("adresse MAC enregistrée");

                            //attribution de la place fait à la main dans la bdd
                        });



                    } else {        //si l'adresse MAC existe deja dans la bdd

                        //affichage console
                        console.log("l\'adresse MAC du capteur existe deja dans la bdd");

                        //on laisse l'adresse du capteur dans la bdd mais on change le nom de sa place (fait a la main)

                    }
                    /*---------------On renvoie ensuite l'ID correspondant a l'adresse MAC---------------*/
                    //On récupère l'ID correspondant dans la bdd
                    mysqlConnection.query("SELECT idCapteur FROM capteur WHERE addrMAC = '" + addMAC + "'", function (err, result) {
                        //variable contenant le resultat de la requete mysql (ID)
                        var resID = result[0];  //on stocke le resultat de la requete mysl dans resID

                        //variable pour utiliser la valeur récupéré par la requete (et non pas la valeur de la requete entière)
                        var valresID = resID.idCapteur

                        //convertie en string
                        valresID = valresID.toString()

                        console.log("valResID")
                        console.log(valresID);

                        //On publie sur ce topic L'ID correspondant à l'@MAC (addMAC)
                        client.publish(T_GIVE_ID, valresID)
                    });
                });
            }


            //si le topic sur lequel le message est publié est T_OCCUPATION_PLACE
            if (topic == "test") {                                             /*---!!!CHANGER TOPIC!!!---*/

                //vérification si place existe dans la base de données (idPlace) ET est assigné à une occupation de place (dans logoccupation) !
                mysqlConnection.query("SELECT capteur.idCapteur, place.idCapteur, place.idPlace, logoccupation.idPlace, logoccupation.idOccupation" +
                    "FROM capteur, place, logoccupation WHERE capteur.idCapteur = '" + idRecu + "' AND capteur.idCapteur = place.idCapteur" +
                    "AND place.idPlace = place.idCapteur AND logoccupation.idOccupation = logoccupation.idPlace", function (err, result) {

                        if (err) console.log("erreur dans la requete")

                        //si il existe:
                        if (result != undefined) {
                            //on stock l'etat de la place dans logoccupation

                        }



                        //on calcul le nombre de place libre dans le parking

                        //on publie sur le topic T_AFF_LOCAUX le nombre de place libre

                        //sinon, on envoie un message d'erreur a la console:    
                    });
            }

        });
    });
    //----------------------OCCUPATION PLACE---------------------------
    //on s'abonne au topic occupationPlace pour récupérer l'etat de la place du capteur 
    client.subscribe(T_OCCUPATION_PLACE, function (err) {

        //lorsque l'on recoit un message:       (le message recu sera en JSON)
        client.on('message', function (topic, message) {
            var idRecu = message;
            var etatPlace;

            console.log(idRecu)
            
        });
    });
});