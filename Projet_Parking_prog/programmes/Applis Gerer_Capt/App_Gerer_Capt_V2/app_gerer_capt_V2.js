/*----------------------------------------------------------------
nom du porgramme:   app_gerer_capt

utilité:    ce programme va servir à:   -configurer les capteurs de place en attribuant à chacun d'eux un ID unique (en fonction de son adresse MAC)
                                        -

Auteur:

date de mise à jours:


----------------------------------------------------------------*/
//-------------------------CONSTANTE------------------------------
//constante pour la connexion MQTT 
const ADDR_SERV_MQTT = 'mqtt://192.168.1.37';
//const ADDR_SERV_MQTT = 'mqtt://172.16.129.22';
const PORT_MQTT = 1883;
const USER_MQTT = '';
const MDP_MQTT = '';

//constante pour la connexion à la base de donnée local
const ADDR_HOTE_BDD = 'localhost';
const USER_BDD = 'root';
const MDP_BDD = 'raspberry';
const NOM_BDD = 'bddparkinglocal'

//constante pour les Topics

/*--------Premiere configuration---------*/
//pour la première configuration @MAC -> BDD
const T_GIVE_ME_MAC = 'giveMAC';
//pour la première configuration BDD -> ID
const T_GIVE_ID = 'giveID'

/*--------Phase Nominale---------*/
//pour l'etat des places (libre ou occupé)
const T_OCCUPATION_PLACE = 'occupationPlace';

//pour afficher sur les afficheurs
const T_AFF_LOCAUX = 'afficheursLocaux';

//topic de test
const T_TEST = 'test'

//-----------------------INCLUDES--------------------------

//inclure MQTT
var mqtt = require('mqtt');

//inclure MySQL
var mysql = require('mysql');

//-----------------------------VARIABLES------------------------------


//---------------------CREATION DES CLIENTS----------------------------

//création d'un client pour se connecter au serveur sur un cerain port et avec un nom d'utilisateur et un mot de passe en mqtt
var client = mqtt.connect(ADDR_SERV_MQTT)

//création d'une connexion mysql pour pouvoir utiliser la BDD
var mysqlConnection = mysql.createConnection({
    host: ADDR_HOTE_BDD,
    user: USER_BDD,
    password: MDP_BDD,
    database: NOM_BDD
});

//--------------------------CONNEXIONS ETABLIES------------------------

//lorsque la connexion avec la BDD est établie:
mysqlConnection.connect(function () {
    console.log("Connexion avec la bdd établie!");      //affichage message
});


//lorsque la connexion avec le serveur MQTT est établie: 
client.on('connect', function () {
    console.log("Connexion au serveur MQTT établi!");


    //on s'abonne au topic giveMAC pour récupérer l'adresse MAC du capteur               MAC -> ID
    client.subscribe(T_GIVE_ME_MAC, function (err) {

        //si il y a une erreur
        if (err) console.log("erreur lors de l'abonnement du topic" + T_GIVE_ME_MAC);

    })

    //on s'abonne au topic occupationPlace pour récupérer l'etat de la place du capteur     OCCUPATION PLACE
    client.subscribe(T_OCCUPATION_PLACE, function (err) {

        if (err) console.log("probleme d'abonnement au topic test")

    })

})

//--------------------RECEPTION DE MESSAGE----------------------
//lorsque l'on recoit un message:
client.on('message', function (topic, message) {

    //si le topic sur lequel le message est publié est T_GIVE_ME_MAC                    /*-----PENSER A SUPPRIMER L4ETAT DE LA PLACE, PUISQU4IL S4AGIT D4UNE NOUVELLE PLACE-----*/
    if (topic === T_GIVE_ME_MAC) {

        //variable contenant l'adresse MAC recu en MQTT (message)
        var addMAC = message.toString();    //stocker dans addMAC l'adresse MAC recu en MQTT 

        //on affiche le topic
        console.log("le topic est: ");
        console.log(topic.toString());

        //on affiche l'adresse MAC
        console.log("l'adresse MAC est: ");
        console.log(addMAC);

        //on stock dans la bdd l'adresse MAC envoyé par le capteur si elle n'existe pas deja 
        //on regarde si l'addrMAC recu en MQTT existe dans la bdd
        mysqlConnection.query("SELECT addrMAC FROM capteur WHERE addrMAC = '" + addMAC + "' ", function (err, result) {

            //variable contenant le resultat de la requete mysql
            var resAddMac = result[0];  //on stocke le resultat de la requete mysl dans resAddMac


            //resultat de la requete:
            console.log("le resultat de la requete est: ");
            console.log(resAddMac);

            //si l'adresse MAC n'est pas stockée ds la bdd,
            if (resAddMac == undefined) {

                //affichage console
                console.log("l\'adresse MAC du capteur n\'est pas enregistrée dans la bdd...");

                //On stocke l'adresse dans la bdd et on lui attribue une place
                //insertion de l'adresse MAC dans la bdd (avec date d'association)
                mysqlConnection.query("INSERT INTO capteur (addrMAC, dateAssoc) VALUES ('" + addMAC + "',DATE( NOW() ) )", function (err, result) {
                    if (err) console.log(err);
                    console.log("adresse MAC enregistrée");

                    //attribution de la place fait à la main dans la bdd
                });



            } else {        //si l'adresse MAC existe deja dans la bdd

                //affichage console
                console.log("l\'adresse MAC du capteur existe deja dans la bdd");

                //on laisse l'adresse du capteur dans la bdd mais on change le nom de sa place (fait a la main)

            }
            /*---------------On renvoie ensuite l'ID correspondant a l'adresse MAC---------------*/
            //On récupère l'ID correspondant dans la bdd
            mysqlConnection.query("SELECT idCapteur FROM capteur WHERE addrMAC = '" + addMAC + "'", function (err, result) {
                //variable contenant le resultat de la requete mysql (ID)
                var resID = result[0];  //on stocke le resultat de la requete mysl dans resID

                //variable pour utiliser la valeur récupéré par la requete (et non pas la valeur de la requete entière)
                var valresID = resID.idCapteur

                //convertie en string
                valresID = valresID.toString()

                console.log("valResID")
                console.log(valresID);

                //On publie sur ce topic L'ID correspondant à l'@MAC (addMAC)
                client.publish(T_GIVE_ID, valresID)
            });
        });
    }



    //si le topic sur lequel le message est publié est T_OCCUPATION_PLACE           (MESSAGE EN JSON)
    if (topic === T_OCCUPATION_PLACE) {

        var idCapteur = message     //variable contenant lID du capteur
        var etatPlace = 'libre'           //variable contenant l'etat de la place envoyé depuis le capteur
        var estLibre             //variable contenant 1  ou  0  en fonction de l'etat le la place
        var nbEtages        //variable contenant le nombre d'etage dans le parking


        //Conditions permettant de determiner si la place est libre ou non, en fonction de l'etat de la place
        if (etatPlace === 'occupe') estLibre = 0;
        if (etatPlace === 'libre') estLibre = 1;


        console.log("le message recu est : " + message)

        /*vérification si la place correspondant à l'etat du capteur existe dans la base de données (idPlace) 
        ET est assigné à une occupation de place (dans logoccupation)*/
        mysqlConnection.query("SELECT capteur.idCapteur, place.nom, place.idPlace FROM capteur, place" +
            " WHERE capteur.idCapteur = '" + idCapteur + "'  AND capteur.idCapteur = place.idCapteur", function (err, result) {

                //message d'erreur
                if (err) console.log("erreur dans la requete" + err)

                //affichage du résultat
                console.log(result[0])

                ////SI l'id du capteur existe et est assigné à une place:
                if (result[0] != undefined) {

                    var idPlace = result[0].idPlace         //variable contenant l'ID de la place du capteur
                    var nomPlace = result[0].nom            //variable contenant le nom de la place

                    //on met a jour l'etat de la place dans place.libre  (1 -> true    /   0 -> false)
                    mysqlConnection.query("UPDATE place SET libre = " + estLibre + " WHERE idPlace = '" + idPlace + "'", function (err, result) {
                        if (err) {
                            console.log("erreur dans la requete" + err)
                        } else {
                            console.log("l'etat de la place: " + nomPlace + " à été mis a jour.")
                        }

                    })

                    //On insert dans la table logOccupation l'etat de la place (afin de garder une trace de l'activité des capteurs) 
                    mysqlConnection.query("INSERT INTO logoccupation (etat, idPlace) VALUES ('" + etatPlace + "','" + idPlace + "')", function (err, result) { })


                } else {                            //SINON

                    //Affichage
                    console.log("Le capteur n'est pas assignée à une place. Pour ce faire, rendez-vous dans la base de données ")
                    console.log("Si le capteur n'est pas enregistré dans la base de données. Appuyer sur le bouton du capteur.")

                }
            });

        //---Calcul du nombre de place libre dans chaques etages---


        //objet qui contiendra l'etage, l'allee et la place courant


        


        var stringEtages = {
            etages: [{ test: 5 }]
        }

        var stringEtagesJSON = ""

        console.log("test")
        /*console.log(stringEtages)
        console.log(stringEtages.etages)
        console.log(stringEtages.etages[0])
        console.log(stringEtages.etages[0].test)*/

        console.log(stringEtages.etages.push({ test: 2 }))




        //on va inserer les lignes dans etages






        //on attend que la place soit mise à jour       //attente de 1.5 seconde
        setTimeout(function () {

            //selectionne l'ID de tous les etages
            mysqlConnection.query("SELECT idEtage FROM etage", function (err, result) {

                //pour chaque etage, 
                for (var i = 0; i < result.length; i++) {

                    console.log(result[i].idEtage)

                    //on fait la somme des places dans chaque etages (on utilise place.libre car c'est un booleen, si la place est libre: libre = 1)
                    mysqlConnection.query("SELECT SUM(place.libre) AS nbPlacesDispo, allee.idEtage FROM allee, etage, place WHERE allee.idAllee = place.idAllee AND etage.idEtage = allee.idEtage AND allee.idEtage = '" + result[i].idEtage + "'", function (err, result2) {

                        //test
                        console.log("test2")
                        console.log(result2[0].idEtage)

                        //on rajoute dans stringEtages.etages, l'idEtage et le nb de place dispo dans cette etage         ex:{ idEtage: '2', nbPlaceDispo: '50' } 
                        stringEtages.etages.push({ idEtage: result2[0].idEtage, nbPlaceDispo: result2[0].nbPlacesDispo })

                    })
                }
                //on attend que stringEtages.etage soit complet
                setTimeout(function () {        //attente de 1.5 seconde
                    console.log("test3")
                    console.log(stringEtages)

                    //on convertit notre valeur javaScript (stringEtages) en chaine JSON (stringEtagesJSON)
                    stringEtagesJSON = JSON.stringify(stringEtages)
                    console.log(stringEtagesJSON)

                    //on publie sur le topic T_AFF_LOCAUX la chaine JSON récupéré
                    client.publish(T_AFF_LOCAUX, stringEtagesJSON)

                }, 1500)
            })
        }, 1500)    
    }
});





/*exemple
        myObj = {
            "name":"John",
            "age":30,
            "cars": [
              { "name":"Ford", "models":[ "Fiesta", "Focus", "Mustang" ] },
              { "name":"BMW", "models":[ "320", "X3", "X5" ] },
              { "name":"Fiat", "models":[ "500", "Panda" ] }
            ]
           }*/

        /*//ce que l'on souhaite avoir
        var objJSON = '{"etages": [{"idEtage": "2", "nbPlaceDispo": "50"},{"idEtage": "2", "nbPlaceDispo": "50"},' +
            '{"idEtage": 2, "nbPlaceDispo": "50"}]}'
          
            var objJS = JSON.parse(objJSON)
            */

        /*
                var myJS = {
                    etages:
                        [
                            { idEtage: '2', nbPlaceDispo: '50' },
                            { idEtage: '2', nbPlaceDispo: '50' },
                            { idEtage: 2, nbPlaceDispo: '50' }
                        ]
                }
        
                console.log(myJS)
        */


