#include <ESP8266WiFi.h>    //connexion au wifi
#include <PubSubClient.h>   //connexion au broker MQTT

//WIFI
const char* ssid = "Reseau ringot";   //Identifiant Wifi
const char* password = "azerty1234";  // Mot de passe WiFi
const char* mqttServer = "hairdresser.cloudmqtt.com";   //IP serveur MQTT
const int mqttPort = 17722;     //Port d'envoi  
const char* mqttUser = "xipndvia";     // User MQTT (si il n'y en a pas, mettre NULL)  
const char* mqttPassword = "q3EQKtDo4Oeu";     //Mot de passe (si il n'y en a pas, mettre NULL)  

//LED on ESP8266 GPIO14 / D5
const int lightPin = D5;

//TOPIC
const char* nomTopic = "/test/light";

//signature de la fonction de callback
void callback(const char* topic, byte* payload, unsigned int length);

//Client 
WiFiClient wifiClient; 
PubSubClient client(mqttServer, mqttPort, callback, wifiClient);

//Code d'erreur
int codErr;


void setup() {
  //Initialisation de la LED en sortie et la LED éteinte
  pinMode(lightPin, OUTPUT)
  digitalWrite(lightPin, LOW);
  
  //démarage connexion serie
  Serial.begin(115200); 
  Serial.println("");

  // Démarrage de la connection WIFI  
  WiFi.begin(ssid, password);
  Serial.print("Connecting");
  
  // on attend d’être connecté  au WiFi avant de continuer
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  //Connexion WiFi établie
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid); 

  //connexion au serveur MQTT
  while (!client.connected()) {
    Serial.println("Connecting to Edge Gateway ...");
    if (client.connect("EspClient", mqttUser, mqttPassword)){
      Serial.println("Connected");  
    }
  }
     

  //abonnement au topic de la lumière
  client.subscribe(nomTopic);

  //initialisation du codErr
  codErr = client.state();
}

void loop() {
  client.loop();
/*  
  if(codErr != client.state()) {
    codErr == client.state();
    switch(codErr) {
      
      case MQTT_CONNECTION_TIMEOUT :
        Serial.println("the server didn't respond within the keepalive time"); 
      break;

      case MQTT_CONNECTION_LOST  :
        Serial.println(" the network connection was broken"); 
      break;

      case MQTT_CONNECT_FAILED  :
        Serial.println("the network connection failed"); 
      break;

      case MQTT_DISCONNECTED  :
        Serial.println("the client is disconnected cleanly"); 
      break;

      case MQTT_CONNECTED  :
        Serial.println("the client is connected"); 
      break;

      case MQTT_CONNECT_BAD_PROTOCOL  :
        Serial.println("the server doesn't support the requested version of MQTT"); 
      break;

      case MQTT_CONNECT_BAD_CLIENT_ID  :
        Serial.println("the server rejected the client identifier"); 
      break;

       case MQTT_CONNECT_UNAVAILABLE   :
        Serial.println("the server was unable to accept the connection"); 
      break;

       case MQTT_CONNECT_BAD_CREDENTIALS   :
        Serial.println("the username/password were rejected"); 
      break;

       case MQTT_CONNECT_UNAUTHORIZED   :
        Serial.println("the client was not authorized to connect"); 
      break;
    }
  }
  while (!client.connected()) {
    Serial.println("Connecting to Edge Gateway ...");
    if (client.connect("EspClient", mqttUser, mqttPassword)){
      Serial.println("Connected");  
    } 
  }
  */     
}



void callback(const char* topic, byte* payload, unsigned int length) {

  
  //convert topic to string to make it easier to work with
  String topicStr = topic; 

  //Print out some debugging info
  Serial.println("Callback update.");
  Serial.print("Topic: ");
  Serial.println(topicStr);

  //turn the light on if the payload is '1' and publish to the MQTT server a confirmation message
  if(payload[0] == '1'){
    digitalWrite(lightPin, HIGH);
    client.publish("/test/confirm", "Light On");
    Serial.println("Light On");

  }

  //turn the light off if the payload is '0' and publish to the MQTT server a confirmation message
  else if (payload[0] == '0'){
    digitalWrite(lightPin, LOW);
    client.publish("/test/confirm", "Light Off");
    Serial.println("Light On");
  }
  

}

