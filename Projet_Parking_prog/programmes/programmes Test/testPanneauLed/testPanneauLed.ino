//----INCLUDES-----
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

//-----DEFINE-----
// Color definitions
#define BLACK    0x0000
#define BLUE     0x001F
#define RED      0xF800
#define GREEN    0x07E0
#define CYAN     0x07FF
#define MAGENTA  0xF81F
#define YELLOW   0xFFE0 
#define WHITE    0xFFFF

//LUMINOSITE
#define BRIGHTNESS 75

//MATRIX
#define MATRIX_WIDTH 32      //Largeur en pixel d'une sous Matrice
#define MATRIX_HEIGHT 8      //Hauteur en pixel d'une sous Matrice
#define PIN_AFF D6       //Broche utilisée pour l'afficheur

//création et configuration de la matrix (panneau d'affichage)
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(MATRIX_WIDTH, MATRIX_HEIGHT, PIN_AFF,
  NEO_MATRIX_TOP     + NEO_MATRIX_LEFT +
  NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
  NEO_GRB            + NEO_KHZ800);

//tableau avec différentes couleur utilisable (R, G, B)
const uint16_t colors[] = {
  matrix.Color(255, 0, 0), matrix.Color(0, 255, 0), matrix.Color(0, 0, 255) };


//-----VARIABLES-----
int posX = 0;
int posY = 1;


//-----SETUP-----
void setup() {
  
  //préparation des données pour la sortie
  matrix.begin();           

  //Mise a jour de la luminosité des leds
  matrix.setBrightness(BRIGHTNESS);

  //Mise à jour de la couleur du texte affiché sur le panneau 
  matrix.setTextColor(colors[0]);


  
}

//int x    = matrix.width();
int x    =  0 ;

int pass = 0;

void loop() {

  Serial.println(x);
  
  matrix.fillScreen(0);       //????
  matrix.setCursor(x, 1);

  //mise a jour de la  taille de l'affichage
  //matrix.updateLength(32);

  int nbPlaces = 789;

  //Afficher un message
  //matrix.print(F("Coucou"));
  matrix.print(nbPlaces);
  
  
 /* for (int16_t i = -50; i <-36; i++) {
    for (int16_t j = -50; j < -36; j++) {
      matrix.drawPixel(i, j, colors[0]);
      delay(250);
    }
  }
*/
 
 /* if(--x < -36) {
    x = matrix.width();
    if(++pass >= 3) pass = 0;
    matrix.setTextColor(colors[pass]);
  }*/
  matrix.show();
  delay(300);
}
