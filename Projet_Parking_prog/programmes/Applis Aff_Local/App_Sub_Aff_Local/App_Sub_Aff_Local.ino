//----INCLUDES-----
//Pour l'afficheur LED
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

//Pour le WIFI
#include <ESP8266WiFi.h>    //Pour connexion au wifi

//Pour le broker MQTT
#include <PubSubClient.h>   //Pour connexion au broker MQTT


//CONSTANTES
//WIFI 
const char* ssid = "tpsnir";   //Identifiant Wifi
const char* password = "t2p0s2n0";  // Mot de passe WiFi

//MQTT
const char* mqttServer = "172.16.129.22";   //IP serveur MQTT
const int mqttPort = 1883;            //Port d'envoi MQTT 
const char* mqttUser = "NULL";     // User MQTT (si il n'y en a pas, mettre NULL)  
const char* mqttPassword = "NULL";     //Mot de passe (si il n'y en a pas, mettre NULL)  

//RS232


//TOPIC
const char* T_AFF = "AfficheursLocaux";

//signature de la fonction de callback
void callback(const char* topic, byte* payload, unsigned int length);

//Clients 
WiFiClient wifiClient;    //Wifi
PubSubClient client(mqttServer, mqttPort, callback, wifiClient);    //MQTT

//Code d'erreur
int codErr;

//LUMINOSITE (afficheur)
#define BRIGHTNESS 75

//MATRIX (afficheur)
#define MATRIX_WIDTH 32      //Largeur en pixel de l'afficheur
#define MATRIX_HEIGHT 8      //Hauteur en pixel de l'afficheur
#define PIN_AFF D6       //Broche utilisée pour l'afficheur

//création et configuration de la matrix (panneau d'affichage)
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(MATRIX_WIDTH, MATRIX_HEIGHT, PIN_AFF,
  NEO_MATRIX_TOP     + NEO_MATRIX_LEFT +
  NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
  NEO_GRB            + NEO_KHZ800);

//tableau avec différentes couleur utilisable (R, G, B)
const uint16_t colors[] = {
  matrix.Color(255, 0, 0), matrix.Color(0, 255, 0), matrix.Color(0, 0, 255) };

//-----VARIABLES-----
//position sur le panneau led
int posX = 2;   //Position (abscisse) du message (ici, il est un peu décalé vers la droite)
int posY = 1;   //Position (ordonné) du message (ici, il est un peu décalé vers le bas)

//nombre de place à afficher
String nbPlacesToAff = "";








void setup() {
/*-----CONFIG PANNEAU LED-----*/
  //préparation des données pour la sortie
  matrix.begin();           

  //Mise a jour de la luminosité des leds
  matrix.setBrightness(BRIGHTNESS);

  //Mise à jour de la couleur du texte affiché sur le panneau 
  matrix.setTextColor(colors[0]);       //Rouge

  //on positionne le premier caractère aux coordonées suivante
  matrix.setCursor(posX, posY);  

  //initialisation PIN RS23
  
/*-----CONNEXION SERIE-----*/  
  //démarage connexion serie
  Serial.begin(115200); 
  Serial.println("Bonjour");

/*-----AFFICHAGE ADRESSE MAC-----*/ 
WiFi.mode(WIFI_STA);    //pour mettre en mode Station (cad: pas en point d'accès)
Serial.println(WiFi.macAddress());  //affichage de l'adresse MAC

/*-----CONFIG WIFI-----*/  

  // Démarrage de la connection WIFI  
  WiFi.begin(ssid, password);
  Serial.print("Connecting");
  
  // on attend d’être connecté  au WiFi avant de continuer
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  //Connexion WiFi établie
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid); 

/*-----CONFIG MQTT-----*/
  //connexion au serveur MQTT
  while (!client.connected()) {
    Serial.println("Connecting to Edge Gateway ...");
    if (client.connect("EspClient", mqttUser, mqttPassword)){
      Serial.println("Connected");  
    }
  }
     

  //abonnement au topic pour afficheur 
  client.subscribe(T_AFF);

  //initialisation du codErr
  codErr = client.state();
}







void loop() {
  client.loop();      

}





//-----------------Fonction de Call Back-------------------------
void callback(const char* topic, byte* payload, unsigned int length) {

  // converti le topic en string pour faciliter le travail
  String topicStr = topic; 

  //Concaténation du message recu (payload)
  String messageRecu = "";

  if(topicStr == T_AFF) {

    //Affichage console de la reception
    Serial.println("Reception...");
    Serial.print("Topic: ");
    Serial.println(topicStr);
    Serial.println("Le message est:");


   /* //Réinitialisation de la chaine de caractère       
    nbPlacesToAff = ""; 
    matrix.print(nbPlacesToAff);   
*/
    /*//On affiche sur le panneau LED la chaine vide (réinitialisation de l'affichage) 
    matrix.show(); 
*/
    //on eteint les leds du premier message
    matrix.fillScreen(0);

    //on repositionne le curseur au debut de la ligne
    matrix.setCursor(2, 1);
    //delay(1000);


    //concaténation de la chaine de charactère recu dans la variable messageRecu
    for (int i = 0; i < length; i++) {
      messageRecu +=  char(payload[i]);
    }
    
    //Stockage du résultat dans la variable globale "nbPlacesToAff"   
    nbPlacesToAff = messageRecu;
   
    //Affichage console
    Serial.println(nbPlacesToAff);

    matrix.print(nbPlacesToAff);    //pour l'affichage sur le panneau LED
  
    //On affiche sur le panneau LED le nombre de place à afficher
    matrix.show(); 

   // delay(300); 
    
  }
}

