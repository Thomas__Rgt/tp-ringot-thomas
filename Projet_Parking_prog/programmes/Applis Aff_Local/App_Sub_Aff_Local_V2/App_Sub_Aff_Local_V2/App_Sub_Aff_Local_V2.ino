//----INCLUDES-----
//Pour l'afficheur LED
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

//Pour récupéré les messages aux format JSON
#include <ArduinoJson.h>

//Pour le WIFI
#include <ESP8266WiFi.h>    //Pour connexion au wifi

//Pour le broker MQTT
#include <PubSubClient.h>   //Pour connexion au broker MQTT


//CONSTANTES / VARIABLES / INSTANCES
//WIFI 
//const char* ssid = "tpsnir";   //Identifiant Wifi
//const char* password = "t2p0s2n0";  // Mot de passe WiFi
const char* ssid = "Livebox-C352";   //Identifiant Wifi
const char* password = "Kcxhhv6QvtntwMqnwk";  // Mot de passe WiFi
WiFiClient wifiClient;    //Client Wifi

//MQTT
//const char* mqttServer = "172.16.129.22";   //IP serveur MQTT
const char* mqttServer = "192.168.1.37";   //IP serveur MQTT
const int mqttPort = 1883;            //Port d'envoi MQTT 
const char* mqttUser = "NULL";     // User MQTT (si il n'y en a pas, mettre NULL)  
const char* mqttPassword = "NULL";     //Mot de passe (si il n'y en a pas, mettre NULL)

//signature de la fonction de callback
void callback(const char* topic, byte* payload, unsigned int length);

PubSubClient client(mqttServer, mqttPort, callback, wifiClient);    //Client MQTT  

//RS232


//TOPIC
const char* T_AFF = "afficheursLocaux";


//Code d'erreur
int codErr;

//LUMINOSITE (afficheur)
#define BRIGHTNESS 75

//MATRIX (afficheur)
#define MATRIX_WIDTH 32      //Largeur en pixel de l'afficheur
#define MATRIX_HEIGHT 8      //Hauteur en pixel de l'afficheur
#define PIN_AFF D6       //Broche utilisée pour l'afficheur

//création et configuration de la matrix (panneau d'affichage)
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(MATRIX_WIDTH, MATRIX_HEIGHT, PIN_AFF,
  NEO_MATRIX_TOP     + NEO_MATRIX_LEFT +
  NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
  NEO_GRB            + NEO_KHZ800);

//tableau avec différentes couleur utilisable (R, G, B)
const uint16_t colors[] = {
  matrix.Color(255, 0, 0), matrix.Color(0, 255, 0), matrix.Color(0, 0, 255) };

//position sur le panneau led
int posX = 2;   //Position (abscisse) du message (ici, il est un peu décalé vers la droite)
int posY = 1;   //Position (ordonné) du message (ici, il est un peu décalé vers le bas)

//nombre de place à afficher
String nbPlacesToAff = "";
String nomEtage = "";








void setup() {
/*-----CONFIG PANNEAU LED-----*/
  //préparation des données pour la sortie
  matrix.begin();           

  //Mise a jour de la luminosité des leds
  matrix.setBrightness(BRIGHTNESS);

  //Mise à jour de la couleur du texte affiché sur le panneau 
  matrix.setTextColor(colors[0]);       //Rouge

  //on positionne le premier caractère aux coordonées suivante
  matrix.setCursor(posX, posY);  

  //initialisation PIN RS23
  
/*-----CONNEXION SERIE-----*/  
  //démarage connexion serie
  Serial.begin(115200); 
  Serial.println("Bonjour");

/*-----AFFICHAGE ADRESSE MAC-----*/ 
WiFi.mode(WIFI_STA);    //pour mettre en mode Station (cad: pas en point d'accès)
Serial.println(WiFi.macAddress());  //affichage de l'adresse MAC

/*-----CONFIG WIFI-----*/  

  // Démarrage de la connection WIFI  
  WiFi.begin(ssid, password);
  Serial.print("Connecting");
  
  // on attend d’être connecté  au WiFi avant de continuer
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  //Connexion WiFi établie
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid); 

/*-----CONFIG MQTT-----*/
  //connexion au serveur MQTT
  while (!client.connected()) {
    Serial.println("Connecting to Edge Gateway ...");
    if (client.connect("EspClient", mqttUser, mqttPassword)){
      Serial.println("Connected");  
    }
  }
     

  //abonnement au topic pour afficheur 
  client.subscribe(T_AFF);

  //initialisation du codErr
  codErr = client.state();
}







void loop() {
  client.loop();      

}





//-----------------Fonction de CallBack-------------------------
void callback(const char* topic, byte* payload, unsigned int length) {

  //objet qui contiendra le message JSON recu de la passerelle edge parsé (deserialisé) 
  DynamicJsonDocument objParsed(200);

  //const char* infoEtage;

  //nom de l'etage du parking
  String nomEtage = "Niveau 1";
  
  //nombre de place disponible dans le parking
  int nbPlacesDispo;

  // converti le topic en string pour faciliter le travail
  String topicStr = topic; 

  //variable pour stocker le message recu (payload) ds une chaine de caractère (initialisé à vide)
  String messageRecu = "";

  //SI le message recu est publié sur le topic T_AFF
  if(topicStr == T_AFF) {

    //Affichage console de la reception
    Serial.println("Reception...");
    Serial.print("Topic: ");
    Serial.println(topicStr);
    Serial.println("données recu de la passerelle edge:");
    
    //on met les leds de l'écran en position éteinte
    matrix.fillScreen(0);

    //on repositionne le curseur au debut de la ligne
    matrix.setCursor(2, 1);

    //concaténation de la chaine de charactère recu dans la variable messageRecu
    for (int i = 0; i < length; i++) {
      messageRecu +=  char(payload[i]);
    }

    //on deserialise le message Recu (au format JSON) dans objParsed 
    DeserializationError error = deserializeJson(objParsed, messageRecu);

    //SI il y a des erreurs 
    if (error) {
      Serial.print("deserializeJson() failed: ");
      Serial.println(error.c_str());
      
    } else {      //SINON
      
      //infoEtage = objParsed["infoEtage"];
      if (nomEtage == objParsed["nomEtage"]) {
        nbPlacesDispo = objParsed["nbPlacesDispo"];  

        //Affichage console
        Serial.println("true");
        Serial.println(nbPlacesDispo);

        //AFFICHAGE PANNEAU LED
        matrix.print(nbPlacesToAff);    //pour l'affichage sur le panneau LED
  
        //On affiche sur le panneau LED le nombre de place à afficher
        matrix.show(); 
      }
      
      
    }    
  }
}
